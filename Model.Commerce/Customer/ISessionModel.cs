﻿using Model.Commerce.Customer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Commerce.Customer
{
    public interface ISessionModel
    {
        IUser CurrentUser { get; set; }
        string CurrentBasketId { get; set; }
        string CurrentCheckoutId { get; set; }

        string ProductId { get; }
    }
}
