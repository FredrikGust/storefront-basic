﻿using Model.Commerce.Customer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Commerce.Dto.Customer
{
    public class CompanyDto : ICompany
    {
        public string ExternalId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
