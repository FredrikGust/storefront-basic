﻿using Model.Commerce.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Commerce.Dto.Order
{
    public class OrderItemDto : IOrderItem
    {
        public int? RowNumber { get; set; }
        public string PartNo { get; set; }
        public string Name { get; set; }
        public decimal? QtyOrdered { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? UnitDiscount { get; set; }
        public decimal? RowAmount { get; set; }
        public decimal? VatRate { get; set; }
        public string SkuTypeExternalId { get; set; }
        public string Status { get; set; }
        public decimal? ParentRowNumber { get; set; }
    }
}
