﻿using Model.Commerce.Customer;
using Model.Commerce.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Commerce.Dto.Order
{
    public class OrderDto : IOrder
    {
        public string ExternalId { get; set; }
        public string OrderNo { get; set; }
        public DateTime? OrderDate { get; set; }
        public string Status { get; set; }
        public string Currency { get; set; }
        public List<IOrderItem> Items { get; set; }
        public ICustomer SellTo { get; set; }
        public ICustomer BillTo { get; set; }
        public ICustomer ShipTo { get; set; }
        public decimal? OrderTotalIncVat { get; set; }
        public decimal? OrderTotalExVat { get; set; }
        public string PaymentMethod { get; set; }
        public string DeliveryMethod { get; set; }
        public string OrderReference { get; set; }
    }
}
