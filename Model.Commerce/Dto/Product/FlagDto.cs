﻿using Model.Commerce.Product;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Commerce.Dto.Product
{
    public class FlagDto : IFlag
    {
        public string ExternalId { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string TypeOfFlag { get; set; }
    }
}
