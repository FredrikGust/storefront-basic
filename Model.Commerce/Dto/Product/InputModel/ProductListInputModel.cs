﻿using Model.Commerce.Product.InputModel;
using System;
using System.Collections.Generic;
using System.Text;
/******************************************************************************
 ** Author: Fredrik Gustavsson, Jolix AB, www.jolix.se
 ** Purpose: Sample code for how to build an integration from a frontend
 **          solution to communicate with Storm Commerce (storm.io)
 ** Copyright (C) Jolix AB, Storm Commerce AB
 ******************************************************************************/
namespace Model.Commerce.Dto.Product.InputModel
{
    public class ProductListInputModel : IProductListInputModel
    {
        public const string SORTBY_NAME_ASC = "Name Asc";
        public const string SORTBY_NAME_DESC = "Name Desc";
        public const string SORTBY_POPULARITY = "PopularityRank";
        public const string SORTBY_PRICE_ASC = "Price Asc";
        public const string SORTBY_PRICE_DESC = "Price Desc";
        public const string SORTBY_SORTORDER = "SortOrder";

        public List<string> CategoryIds { get; set; }
        public List<string> FlagIds { get; set; }
        public List<string> ManufacturerIds { get; set; }
        public List<string> ParametricsFilters { get; set; }
        public List<IFilter> Filters { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public string Query { get; set; }
        public string Sort { get; set; }
    }
}
