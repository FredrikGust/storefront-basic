﻿using Model.Commerce.Product.InputModel;
using System;
using System.Collections.Generic;
using System.Text;
/******************************************************************************
 ** Author: Fredrik Gustavsson, Jolix AB, www.jolix.se
 ** Purpose: Sample code for how to build an integration from a frontend
 **          solution to communicate with Storm Commerce (storm.io)
 ** Copyright (C) Jolix AB, Storm Commerce AB
 ******************************************************************************/
namespace Model.Commerce.Dto.Product.InputModel
{
    public class FilterDto : IFilter
    {
        public FilterType FilterType { get; set; }
        public string ExternalId { get; set; }
        public string Value { get; set; }
    }
}
