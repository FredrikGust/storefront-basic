﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Commerce.Order
{
    public interface IOrderItem
    {
        int? RowNumber { get; set; }
        string PartNo { get; set; }
        string Name { get; set; }
        decimal? QtyOrdered { get; set; }
        decimal? UnitPrice { get; set; }
        decimal? UnitDiscount { get; set; }
        decimal? RowAmount { get; set; }
        decimal? VatRate { get; set; }
        string SkuTypeExternalId { get; set; }
        string Status { get; set; }
        decimal? ParentRowNumber { get; set; }

    }
}
