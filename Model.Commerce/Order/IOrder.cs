﻿using Model.Commerce.Customer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Commerce.Order
{
    public interface IOrder
    {
        string ExternalId { get; set; }
        string OrderNo { get; set; }
        DateTime? OrderDate { get; set; }
        string Status { get; set; }
        string Currency { get; set; }
        List<IOrderItem> Items { get; set; }
        ICustomer SellTo { get; set; }
        ICustomer BillTo { get; set; }
        ICustomer ShipTo { get; set; }
        decimal? OrderTotalIncVat { get; set; }
        decimal? OrderTotalExVat { get; set; }
        string PaymentMethod { get; set; }
        string DeliveryMethod { get; set; }
        string OrderReference { get; set; }

    }
}
