﻿using Model.Commerce.Customer;
using Model.Commerce.Product;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Commerce.Extensions
{
    public interface ICommerceDataExtension
    {

        IFlag FindFlagById(IUser user, string externalId);
        ICategory FindCategoryById(IUser user, string externalId);
        IAttribute FindAttributeById(IUser user, string externalId);
        IAttributeValue FindAttributeValueById(IUser user, string attributeExternalId, string attributeValueExternalId);
        IManufacturer FindManufacturerById(IUser manufacturer, string externalId);

    }
}
