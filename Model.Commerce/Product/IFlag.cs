﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Commerce.Product
{
    public interface IFlag
    {
        string ExternalId { get; }
        string Code { get; }
        string Name { get; }
        string TypeOfFlag { get; }
    }
}
