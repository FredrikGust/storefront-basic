﻿using Integration.Storm.Builder;
using Integration.Storm.Model.Order;
using Microsoft.Extensions.Configuration;
using Model.Commerce.Customer;
using Model.Commerce.Managers;
using Model.Commerce.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Storm.Managers
{
    public class StormOrderManager : IOrderManager
    {
        private IStormConnectionManager _connectionManager;
        private IConfiguration _configuration;

        public StormOrderManager(IStormConnectionManager connectionManager, IConfiguration configuration)
        {
            _connectionManager = connectionManager;
            _configuration = configuration;
        }


        public IOrder FindOrderByBasketId(IUser currentUser, string basketExternalId)
        {

            string url = $"OrderService.svc/rest/GetOrderByBasket?basketId={basketExternalId}&cultureCode={currentUser.LanguageCode}";

            var stormOrder = _connectionManager.GetResult<StormOrder>(url);

            if (stormOrder == null) return null;
            OrderBuilder orderBuilder = new OrderBuilder(_configuration);

            return orderBuilder.BuildOrder(stormOrder);
        }

    }
}
