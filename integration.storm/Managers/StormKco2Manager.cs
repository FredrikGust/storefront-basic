﻿using Integration.Storm.Model.Shopping;
using Microsoft.Extensions.Configuration;
using Model.Commerce.Customer;
using Model.Commerce.Dto.Product;
using Model.Commerce.Dto.Shopping;
using Model.Commerce.Managers;
using Model.Commerce.Shopping;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
/******************************************************************************
 ** Author: Fredrik Gustavsson, Jolix AB, www.jolix.se
 ** Purpose: Sample code for how to build an integration from a frontend
 **          solution to communicate with Storm Commerce (storm.io)
 ** Copyright (C) Jolix AB, Storm Commerce AB
 ******************************************************************************/
namespace Integration.Storm.Managers
{
    public class StormKco2Manager: IFormCheckoutProvider
    {
        IStormConnectionManager _stormConnectionManager;
        IProductManager _productManager;
        IConfiguration _configuration;

        const string FORM_CHECKOUT_PROVIDER = "KCOv2";

        public StormKco2Manager(IStormConnectionManager connectionManager, IProductManager productManager, IConfiguration configuration)
        {
            _stormConnectionManager = connectionManager;
            _productManager = productManager;
            _configuration = configuration;
        }

        public IPaymentResponse PaymentComplete(string reference, string basketId)
        {
            List<StormNameValue> list = new List<StormNameValue>();
            list.Add(new StormNameValue() { Name = "klarna_checkoutId", Value = reference });
            list.Add(new StormNameValue() { Name = "PaymentService", Value = "KlarnaCheckout" });

            PaymentResponseDto dto = new PaymentResponseDto();
            try
            { 
                string url = $"ShoppingService.svc/rest/PaymentFormCallback?format=json&basketId={basketId}&cultureCode=sv-SE";
                var paymentResponse = _stormConnectionManager.PostResult<StormPaymentResponse>(url, list);

                dto.Html = paymentResponse.PaymentReference;
                dto.FormCheckoutProvider = FORM_CHECKOUT_PROVIDER;
            }
            catch( Exception e )
            {
                dto.Html = "Kolla i din e-post efter orderbekräftelse <!-- " + e.Message + " -->";
                dto.FormCheckoutProvider = FORM_CHECKOUT_PROVIDER;
            }
            return dto;
        }

        public IPaymentResponse PaymentForm(IUser currentUser, string basketId)
        {
            var baseUrl = _configuration["Storm:BaseUrl"];

            List<StormNameValue> list = new List<StormNameValue>();
            list.Add(new StormNameValue() { Name = "mobile", Value = "false" });
            list.Add(new StormNameValue() { Name = "termsurl", Value = baseUrl + "/info/ordervillkor" });
            list.Add(new StormNameValue() { Name = "checkouturl", Value = baseUrl + "/f/Checkout" });
            list.Add(new StormNameValue() { Name = "confirmationurl", Value = baseUrl + "/f/order-complete" });

            string url = $"ShoppingService.svc/rest/GetPaymentForm?format=json";

            url += "&basketId=" + basketId;
            url += "&ipAddress=" + "172.16.98.1";
            url += "&userAgent=" + "unknown";

            if( currentUser.PriceLists != null )
            {
                url += "&priceListSeed=" + string.Join(",", currentUser.PriceLists);
            }


            var paymentResponse = _stormConnectionManager.PostResult<StormPaymentResponse>(url, list);

            PaymentResponseDto dto = new PaymentResponseDto();

            dto.Reference = paymentResponse.RedirectUrl;
            dto.Html = paymentResponse.PaymentReference;
            dto.FormCheckoutProvider = FORM_CHECKOUT_PROVIDER;

            return dto;
        }

    }
}
