﻿using Integration.Storm.Model.Product;
using Microsoft.Extensions.Configuration;
using Model.Commerce.Customer;
using Model.Commerce.Dto.Product;
using Model.Commerce.Extensions;
using Model.Commerce.Managers;
using Model.Commerce.Product;
using Model.Commerce.Product.InputModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

/******************************************************************************
 ** Author: Fredrik Gustavsson, Jolix AB, www.jolix.se
 ** Purpose: Sample code for how to build an integration from a frontend
 **          solution to communicate with Storm Commerce (storm.io)
 ** Copyright (C) Jolix AB, Storm Commerce AB 123
 ******************************************************************************/

namespace Integration.Storm.Managers
{
    public class StormProductManager : IProductManager
    {
        private IProductBuilder<StormProductItem, StormProduct> _productBuilder;
        private IBuyableExtension _buyableExtension;
        private IStormConnectionManager _connectionManager;
        private IConfiguration _configuration;

        private int PageSize = 50;

        public StormProductManager(IStormConnectionManager connectionManager,  IProductBuilder<StormProductItem,StormProduct> productBuilder, IBuyableExtension buyableExtension, IConfiguration configuration)
        {
            _productBuilder = productBuilder;
            _buyableExtension = buyableExtension;
            _connectionManager = connectionManager;
            _configuration = configuration;

            PageSize = Convert.ToInt32(_configuration["Storm:DefaultPageSize"]);
        }

        public IProductList FindByCategory(IUser currentUser, IProductListInputModel query)
        {

            // Find the list of products
            string url = "ProductService.svc/rest/ListProducts2?";

            url += addUserUrlDetails(currentUser);

            url += "&statusSeed=" + _configuration["Storm:StatusSeed"];

            if ( query.CategoryIds != null ) {
                url += "&categorySeed=" + string.Join(",", query.CategoryIds);
            }

            if( query.FlagIds != null )
            {
                url += "&flagSeed=" + string.Join(",", query.FlagIds); 
            }
            if( !string.IsNullOrEmpty(query.Query))
            {
                url += "&searchString=" + System.Web.HttpUtility.UrlEncode(query.Query);
            }
            if (query.ParametricsFilters != null)
            {
                url += "&parametrics=" + string.Join("*", query.ParametricsFilters);
            }

            if( query.ManufacturerIds != null )
            {
                url += "&manufacturerSeed=" + string.Join(",", query.ManufacturerIds);
            }

            if( query.Filters != null )
            {
                url += "&filter=" + buildFilters(query.Filters);
            }

            if( query.Query != null )
            {
                url += "&searchString=" + HttpUtility.UrlEncode(query.Query);
            }

            url += "&pageNo=" + (query.PageNumber > 0 ? query.PageNumber : 1);
            url += "&pageSize=" + (query.PageSize>0?query.PageSize:PageSize);
            url += "&asVariants=1";

            if( !string.IsNullOrEmpty(query.Sort))
            {
                url += "&sort=" + query.Sort;
            }

            var productList = _connectionManager.GetResult<StormProductList>(url);

            ProductListDto result = parseProductList(productList, (query.PageNumber > 0 ? query.PageNumber : 1));

            return result;
        }

        private ProductListDto parseProductList(StormProductList productList, int pageNumber=1)
        {
            var result = new ProductListDto();
            result.ProductCount = productList.ItemCount;
            result.PageSize = PageSize;
            result.PageNumber = pageNumber;
            result.Products = new List<IProduct>();

            Dictionary<string, ProductDto> variants = new Dictionary<string, ProductDto>();
            foreach (var item in productList.Items)
            {
                ProductDto p = (ProductDto)_productBuilder.BuildFromItem(item);

                if (!string.IsNullOrEmpty(p.GroupByKey))
                {
                    if (variants.ContainsKey(p.GroupByKey))
                    {
                        variants[p.GroupByKey].Variants.Add((VariantDto)p.PrimaryVariant);
                    }
                    else
                    {
                        variants[p.GroupByKey] = p;
                        result.Products.Add(p);
                    }

                }
                else
                {
                    result.Products.Add(p);

                }

            }
            return result;
        }

        private string buildFilters(List<IFilter> filters)
        {
            if (filters == null || filters.Count == 0) return string.Empty;

            List<string> parts = new List<string>();

            if( filters.Any(x=>x.FilterType == FilterType.Category))
            {
                parts.Add("catf|" + string.Join(",", filters.Where(x => x.FilterType == FilterType.Category).Select(x => x.Value)));
            }
            if (filters.Any(x => x.FilterType == FilterType.Manufacturer))
            {
                parts.Add("mfrf|" + string.Join(",", filters.Where(x => x.FilterType == FilterType.Manufacturer).Select(x => x.Value)));
            }
            if (filters.Any(x => x.FilterType == FilterType.Flag))
            {
                parts.Add("catf|" + string.Join(",", filters.Where(x => x.FilterType == FilterType.Category).Select(x => x.Value)));
            }
            if (filters.Any(x => (x.FilterType == FilterType.MultiValue) || (x.FilterType == FilterType.ListValue) || (x.FilterType == FilterType.RangeValue)))
            {
                parts.Add("parf|" + string.Join("*", filters.Where(x => (x.FilterType == FilterType.MultiValue) || (x.FilterType == FilterType.ListValue) || (x.FilterType == FilterType.RangeValue)).Select(x => x.Value)));
            }

            return string.Join(";", parts);
        }

        public IList<IProductFilter> FindAllFilters(IUser currentUser, IProductListInputModel query)
        {
            // Find the list of products
            string url = "ProductService.svc/rest/ListProductFilters2?";

            url += addUserUrlDetails(currentUser);

            url += "&statusSeed=" + _configuration["Storm:StatusSeed"];

            if (query.CategoryIds != null)
            {
                url += "&categorySeed=" + string.Join(",", query.CategoryIds);
            }

            if (query.FlagIds != null)
            {
                url += "&flagSeed=" + string.Join(",", query.FlagIds);
            }
            if (!string.IsNullOrEmpty(query.Query))
            {
                url += "&searchString=" + System.Web.HttpUtility.UrlEncode(query.Query);
            }
            if (query.ParametricsFilters != null)
            {
                url += "&parametrics=" + string.Join("*", query.ParametricsFilters);
            }
            if (query.ManufacturerIds != null)
            {
                url += "&manufacturerSeed=" + string.Join(",", query.ManufacturerIds);
            }
            var filterList = _connectionManager.GetResult<List<StormFilter>>(url);

            List<IProductFilter> result = new List<IProductFilter>();
           
            Dictionary<string, ProductDto> variants = new Dictionary<string, ProductDto>();
            foreach ( var item in filterList )
            {

                var dto = new ProductFilterDto();

                dto.Items = new List<IProductFilterItem>();
                dto.Name = item.Type;
                dto.Type = item.Name;

                if( item.Name.Equals("parf"))
                {
                    foreach (var entry in item.Items)
                    {
                        dto = new ProductFilterDto();
                        dto.Items = new List<IProductFilterItem>();
                        dto.Name = entry.Name;
                        dto.Type = item.Name;

                        var filterType = "V";
                        if (entry.TypeClassName == null) filterType = "V";
                        else if (entry.TypeClassName.StartsWith("FilterMultiItem:")) filterType = "M";
                        else if (entry.TypeClassName.StartsWith("FilterListItem:")) filterType = "L";


                        if ( entry.Items != null ) { 
                            foreach( var valuef in entry.Items)
                            {
                                var dtoi = new ProductFilterItem();
                                dtoi.Count = valuef.Count ?? 0;
                                dtoi.Id = valuef.Id;
                                dtoi.Name = valuef.Name;
                                dtoi.Type = filterType + entry.Id;
                                dtoi.Value = valuef.Value;
                                dto.Items.Add(dtoi);
                            }
                        } 
                        else if( entry.FalseCount.HasValue )
                        {
                            var dtoi = new ProductFilterItem();
                            dtoi.Count = entry.Count ?? 0;
                            dtoi.Name = "True";
                            dtoi.Id = "0-0";
                            dtoi.Type = filterType + entry.Id;
                            dto.Items.Add(dtoi);

                            dtoi = new ProductFilterItem();
                            dtoi.Count = entry.FalseCount ?? 0;
                            dtoi.Type = filterType + entry.Id;
                            dtoi.Id = "1-1";
                            dtoi.Name = "False";
                            dto.Items.Add(dtoi);
                        }
                        result.Add(dto);
                    }

                }
                else if (item.Name.Equals("ohf"))
                {
                    result.Add(dto);
                    var dtoi = new ProductFilterItem();
                    dtoi.Count = item.Items[0].Count ?? 0;
                    dtoi.Name = "Onhand";
                    dto.Items.Add(dtoi);
                }
                else { 
                    foreach( var entry in item.Items )
                    {
                        var dtoi = new ProductFilterItem();
                        dtoi.Count = entry.Count??0;
                        dtoi.Id = entry.Id;
                        dtoi.Name = entry.Name;
                        dtoi.Type = entry.Type;
                        dtoi.Value = entry.Value;
                        dto.Items.Add(dtoi);
                    }
                    result.Add(dto);
                }

            }

            return result;
        }


        public IProduct FindByPartNo(IUser currentUser, string partNo)
        {
            // Find the list of products
            string url = "ProductService.svc/rest/GetProductByPartNo?";

            url += addUserUrlDetails(currentUser);

            url += "&partNo=" + partNo;

            var product = _connectionManager.GetResult<StormProduct>(url);
            
            var p = _productBuilder.BuildFromProduct(product);
                
            return p;
        }

        public IProductList FindByPartNos(IUser currentUser, List<string> partNos)
        {
            // Find the list of products
            string url = "ProductService.svc/rest/ListProductsByPartNos?";

            url += addUserUrlDetails(currentUser);
            url += "&partNoSeed=" + string.Join(",", partNos);
            url += "&asVariants=1";

            var productList = _connectionManager.GetResult<StormProductList>(url);

            ProductListDto result = parseProductList(productList);

            return result;
        }


        public IProduct FindByUrl(IUser currentUser, string uniqueName)
        {
            // Find the list of products
            string url = "ProductService.svc/rest/GetProductByUniqueName?";

            url += addUserUrlDetails(currentUser);

            url += "&uniqueName=" + uniqueName;

            var product = _connectionManager.GetResult<StormProduct>(url);
            if (product == null) return null;

            var p = _productBuilder.BuildFromProduct(product);

            return p;
        }

        public IProductList FindAccessories(IUser currentUser, string productExternalId)
        {
            // TODO: Replace with the newser 5
            string url = "ProductService.svc/rest/ListProductAccessories3?";

            url += addUserUrlDetails(currentUser);

            url += "&productid=" + productExternalId;
            url += "&statusSeed=" + _configuration["Storm:StatusSeed"];
            url += "&asVariants=1";

            var productList = _connectionManager.GetResult<StormProductList>(url);

            ProductListDto result = parseProductList(productList);
            return result;
        }
        public IProductList FindUpSell(IUser currentUser, string productExternalId)
        {
            // Find the list of products
            string url = "ProductService.svc/rest/ListProductUpSell2?";

            url += addUserUrlDetails(currentUser);

            url += "&productid=" + productExternalId;
            url += "&statusSeed=" + _configuration["Storm:StatusSeed"];
            url += "&asVariants=1";

            var productList = _connectionManager.GetResult<StormProductList>(url);

            ProductListDto result = parseProductList(productList);
            return result;
        }

        public IProductList FindCrossSell(IUser currentUser, string productExternalId)
        {
            // Find the list of products
            string url = "ProductService.svc/rest/ListProductCrossSell2?";

            url += addUserUrlDetails(currentUser);

            url += "&productid=" + productExternalId;
            url += "&statusSeed=" + _configuration["Storm:StatusSeed"];
            url += "&asVariants=1";

            var productList = _connectionManager.GetResult<StormProductList>(url);

            ProductListDto result = parseProductList(productList);
            return result;
        }

        public IProductList Query(IUser currentUser, string query)
        {
            throw new NotImplementedException();
        }


        private string addUserUrlDetails(IUser currentUser)
        {
            string url = string.Empty;

            if (!string.IsNullOrEmpty(currentUser.ExternalId))
            {
                url += "&customerId=" + currentUser.ExternalId;
            }
            if (currentUser.Company != null && !string.IsNullOrEmpty(currentUser.Company.ExternalId))
            {
                url += "&companyId=" + currentUser.Company.ExternalId;
            }

            if( !string.IsNullOrEmpty(currentUser.CurrencyCode))
            {
                url += "&currencyId=" + currentUser.CurrencyCode;
            }

            if (!string.IsNullOrEmpty(currentUser.LanguageCode))
            {
                url += "&cultureCode=" + currentUser.LanguageCode;
            }

            return url;

        }
    }
}
