﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Storm.Model.Product
{
    public class StormFlag
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int? TypeId { get; set; }
        public int? GroupId { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
    }
}
