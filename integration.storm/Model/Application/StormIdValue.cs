﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Storm.Model.Application
{
    public class StormIdValue
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
    }
}
