﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Storm.Model.Order
{
    public class StormOrderCompany
    {
        public long? Id { get; set; }
        public string No { get; set; }
        public string Name { get; set; }
        public string OrgNo { get; set; }
    }
}
