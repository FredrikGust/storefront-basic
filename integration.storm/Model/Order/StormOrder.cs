﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Storm.Model.Order
{
    public class StormOrder
    {
        public long? Id { get; set; }
        public long? QuotationId { get; set; }
        public string OrderNo { get; set; }
        public DateTime? OrderDate { get; set; }
        public string Status { get; set; }
        public string Source { get; set; }
        public string Currency { get; set; }
        public List<StormOrderItem> Items { get; set; }
        public StormCustomerData SellTo { get; set; }
        public StormCustomerData BillTo { get; set; }
        public StormCustomerData ShipTo { get; set; }
        public decimal? OrderTotalIncVat { get; set; }
        public decimal? OrderTotalExVat { get; set; }
        public string PaymentMethod { get; set; }
        public string DeliveryMethod { get; set; }
        public List<StormDeliveryNote> DeliveryNotes { get; set; }
        public List<StormInfo> Info { get; set; }
        public int? TypeId { get; set; }
        public string DeliveryMark { get; set; }
        public StormCustomerData DropPoint { get; set; }
    }
}
