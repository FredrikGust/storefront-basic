﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Storm.Model.Order
{
    public class StormOrderCustomer
    {
        public long? Id { get; set; }
        public string No { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CellPhone { get; set; }

    }
}
