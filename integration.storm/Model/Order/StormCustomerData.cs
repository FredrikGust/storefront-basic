﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Storm.Model.Order
{
    public class StormCustomerData
    {
        public StormOrderCompany Company { get; set; }
        public StormOrderCustomer Customer { get; set; }
        public StormOrderAddress Address { get; set; }

    }
}
