﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Storm.Model.Order
{
    public class StormInfo
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
    }
}
