﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Storm.Model.Order
{
    public class StormOrderAddress
    {
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string CareOf { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Region { get; set; }
        public string ShippingPhoneNumber { get; set; }
        public string GlobalLocationNumer { get; set; }
    }
}
