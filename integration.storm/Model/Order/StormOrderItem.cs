﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Storm.Model.Order
{
    public class StormOrderItem
    {
        public decimal? RowNumber { get; set; }
        public string PartNo { get; set; }
        public string Name { get; set; }
        public decimal? QtyOrdered { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? UnitDiscount { get; set; }
        public decimal? RowAmount { get; set; }
        public decimal? VatRate { get; set; }
        public Guid? ImageKey { get; set; }
        public int? Type { get; set; }
        public string Status { get; set; }
        public List<StormInfo> Info { get; set; }
        public decimal? ParentRowNumber { get; set; }
        public decimal? PriceRecommended { get; set; }
        public decimal? PriceCatalog { get; set; }
        public decimal? PriceStandard { get; set; }
    }
}
