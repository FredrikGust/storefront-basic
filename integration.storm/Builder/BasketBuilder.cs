﻿using Integration.Storm.Model.Shopping;
using Microsoft.Extensions.Configuration;
using Model.Commerce.Dto.Product;
using Model.Commerce.Dto.Shopping;
using Model.Commerce.Shopping;
using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Storm.Builder
{
    public class BasketBuilder
    {
        private IConfiguration _configuration;

        public BasketBuilder(IConfiguration configuration )
        {
            _configuration = configuration;
        }

        public BasketDto BuildBasketDto(StormBasket basket)
        {
            BasketDto dto = new BasketDto();

            dto.ExternalId = basket.Id.ToString();
            dto.Items = new List<IBasketItem>();
            dto.Shipping = basket.Summary.Freigt.Amount;
            dto.ShippingInclVat = basket.Summary.Freigt.Amount + basket.Summary.Freigt.Vat;
            dto.Total = basket.Summary.Total.Amount;
            dto.TotalInclVat = basket.Summary.Total.Vat + dto.Total;
            dto.TotalVat = basket.Summary.Total.Vat;
            dto.NumberOfItems = 0;
            dto.Buyable = true;
            dto.DiscountAmount = 0;
            dto.DiscountCode = basket.DiscountCode;
            dto.Promotions = new List<string>();

            foreach (var stormItem in basket.Items)
            {

                // Calculate discounts
                if (stormItem.AppliedPromotions != null)
                {
                    foreach (var promotion in stormItem.AppliedPromotions)
                    {
                        dto.DiscountAmount += promotion.AppliedAmount * stormItem.VatRate.Value;
                    }
                }

                if (stormItem.Type.HasValue && _configuration["Storm:ExcludeTypeFromBasket"].Contains(stormItem.Type.Value.ToString()))
                {
                    continue;
                }

                decimal? priceStandard = null;
                if (stormItem.PriceOriginal.HasValue && stormItem.PriceOriginal.Value > 0)
                {
                    priceStandard = stormItem.PriceOriginal.Value;
                }

                BasketItemDto itemdto = new BasketItemDto();
                itemdto.ExternalId = stormItem.Id.ToString();
                itemdto.ImageUrl = _configuration["Storm:ImagePrefix"] + stormItem.ImageKey;
                itemdto.Name = stormItem.Name;
                itemdto.PartNo = stormItem.PartNo;
                itemdto.Quantity = Convert.ToInt32(stormItem.Quantity);
                itemdto.Price = stormItem.PriceDisplay.Value;
                itemdto.PricePrevious = priceStandard;
                itemdto.VatRate = stormItem.VatRate.Value;
                itemdto.Url = stormItem.UniqueName;
                itemdto.Buyable = true;

                dto.NumberOfItems += Convert.ToInt32(itemdto.Quantity);

                itemdto.AvailableToSell = 0;

                // Calculate onhand
                if( stormItem.OnHand != null  && stormItem.OnHand.Value.HasValue )
                {
                    itemdto.AvailableToSell = stormItem.OnHand.Value;
                    itemdto.NextDelivery = stormItem.OnHand.NextDeliveryDate;
                    itemdto.LeadTime = stormItem.OnHand.LeadtimeDayCount ?? 1;
                }

                if( stormItem.OnHandSupplier != null && stormItem.OnHandSupplier.Value.HasValue )
                {
                    itemdto.AvailableToSell += stormItem.OnHandSupplier.Value;
                    itemdto.LeadTime = stormItem.OnHandSupplier.LeadtimeDayCount ?? 5;
                }

                if (itemdto.Quantity > itemdto.AvailableToSell) itemdto.Buyable = false;
                if (stormItem.StatusId.ToString() == VariantDto.STATUS_COMING) itemdto.Buyable = false;
                if (stormItem.StatusId.ToString() == VariantDto.STATUS_INACTIVE) itemdto.Buyable = false;
                if (stormItem.StatusId.ToString() == VariantDto.STATUS_NOTACTIVATED) itemdto.Buyable = false;

                if (!itemdto.Buyable) dto.Buyable = false;

                


                dto.Items.Add(itemdto);
            }

            // Campaigns
            if(  basket.AppliedPromotions != null )
            {
                foreach( var promotion in basket.AppliedPromotions )
                {
                    string text = promotion.Name + " " + promotion.AppliedAmountIncVat.Value;
                    dto.Promotions.Add(text);
                }
            }


            return dto;
        }

    }
}
