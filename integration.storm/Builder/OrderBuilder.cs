﻿using Integration.Storm.Model.Order;
using Microsoft.Extensions.Configuration;
using Model.Commerce.Customer;
using Model.Commerce.Dto.Customer;
using Model.Commerce.Dto.Order;
using Model.Commerce.Order;
using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Storm.Builder
{
    public class OrderBuilder
    {
        private IConfiguration _configuration;

        public OrderBuilder(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IOrder BuildOrder(StormOrder stormOrder)
        {
            OrderDto order = new OrderDto();
            order.ExternalId = stormOrder.Id.Value.ToString();
            order.Currency = stormOrder.Currency;
            order.DeliveryMethod = stormOrder.DeliveryMethod;
            order.OrderDate = stormOrder.OrderDate;
            order.OrderReference = stormOrder.DeliveryMark;
            order.PaymentMethod = stormOrder.PaymentMethod;
            order.DeliveryMethod = stormOrder.DeliveryMethod;
            order.OrderTotalExVat = stormOrder.OrderTotalExVat;
            order.OrderTotalIncVat = stormOrder.OrderTotalIncVat;

            order.Items = new List<IOrderItem>();

            foreach( var stormItem in stormOrder.Items )
            {
                OrderItemDto item = new OrderItemDto();
                item.RowNumber = Convert.ToInt32(stormItem.RowNumber);
                item.PartNo = stormItem.PartNo;
                item.Name = stormItem.Name;
                item.QtyOrdered = stormItem.QtyOrdered;
                item.UnitPrice = stormItem.UnitPrice;
                item.UnitDiscount = stormItem.UnitDiscount;
                item.VatRate = stormItem.VatRate;
                item.SkuTypeExternalId = stormItem.Type.Value.ToString();

                order.Items.Add(item);
            }

            order.SellTo = BuildCustomer(stormOrder.SellTo);
            order.ShipTo = BuildCustomer(stormOrder.ShipTo);
            order.BillTo = BuildCustomer(stormOrder.BillTo);

            return order;
        }

        private ICustomer BuildCustomer(StormCustomerData customerData)
        {
            if (customerData == null) return null;

            CustomerDto customer = new CustomerDto();

            if(customerData.Company != null )
            {
                customer.Company = new CompanyDto();
                customer.Company.Code = customerData.Company.No;
                customer.Company.ExternalId = customerData.Company.Id.HasValue ? customerData.Company.Id.Value.ToString() : null;
                customer.Company.Name = customerData.Company.Name;
            }

            if( customerData.Customer != null ) { 
                customer.FirstName = customerData.Customer.FirstName;
                customer.LastName = customerData.Customer.LastName;
                customer.Email = customerData.Customer.Email;
                customer.ExternalId = customerData.Customer.Id.HasValue ? customerData.Customer.Id.Value.ToString() : null;
                customer.Phone = customerData.Customer.Phone;
                customer.MobilePhone = customerData.Customer.CellPhone;
                customer.Code = customerData.Customer.No;
            }

            if( customerData.Address != null )
            {
                customer.Address1 = customerData.Address.Line1;
                customer.Address2 = customerData.Address.Line2;
                customer.City = customerData.Address.City;
                customer.Zip = customerData.Address.ZipCode;
                customer.Country = customerData.Address.Country ?? customerData.Address.CountryCode;
            }

            return customer;
        }

    }
}
