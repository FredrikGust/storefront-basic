﻿using Model.Commerce.Customer;
using Model.Commerce.Dto.Product;
using Model.Commerce.Extensions;
using Model.Commerce.Managers;
using Model.Commerce.Product;
using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Storm.Extensions
{
    public class DefaultCommerceBaseDataExtension : ICommerceDataExtension
    {
        private IApplicationManager _applicationManager;
        private static readonly Dictionary<string, IFlag> _flags = new Dictionary<string, IFlag>();

        public DefaultCommerceBaseDataExtension(IApplicationManager applicationManager)
        {
            _applicationManager = applicationManager;
        }

        public IAttribute FindAttributeById(IUser user, string externalId)
        {
            return new AttributeDto() { ExternalId = externalId };
        }

        public IAttributeValue FindAttributeValueById(IUser user, string attributeExternalId, string attributeValueExternalId)
        {
            return new AttributeValueDto() {   ExternalId = attributeValueExternalId };
        }

        public ICategory FindCategoryById(IUser user, string externalId)
        {
            return new CategoryDto() { ExternalId = externalId };
        }

        public IFlag FindFlagById(IUser user, string externalId)
        {
            if( _flags.Count == 0 )
            {
                var allFlags = _applicationManager.FindAllFlags(user);
                foreach( var flag in allFlags )
                {
                    _flags[flag.ExternalId] = flag;
                }
            }

            IFlag flagout;
            if (_flags.TryGetValue(externalId, out flagout) )
            {
                return flagout;
            }

            return new FlagDto() { ExternalId = externalId };
        }

        public IManufacturer FindManufacturerById(IUser manufacturer, string externalId)
        {
            return new ManufacturerDto() { ExternalId = externalId };
        }
    }
}
